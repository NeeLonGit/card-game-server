import { Controller, Get, Res } from '@nestjs/common';
import { ANGULAR_ROOT_DIR } from './app.constant';
import * as fs from 'fs';

@Controller()
export class AppController {

    // You'll need a file "app.constant.ts" next to this file.
    //  To serve a frontend application, let it's content be:
    // 
    // export const ANGULAR_ROOT_DIR = "<path-to-dist>/<folder-of-angular-application>";
    // 
    private rootDir = { root: ANGULAR_ROOT_DIR };

    @Get('app')
    public getAngularApp(@Res() res): any {
        res.sendFile('index.html', this.rootDir);
    }
    @Get('favicon.ico')
    public getFavicon(@Res() res): any {
        res.sendFile('favicon.ico', this.rootDir);
    }
    @Get('main-es5.*.js')
    public getMainES5(@Res() res): any {
        const file = fs.readdirSync(this.rootDir.root).filter(file => (file.match('main\-es5\.[^\.]*\.js')))[0];
        console.log(`sending ${file}`);
        res.sendFile(file, this.rootDir);
    }
    @Get('main-es2015.*.js')
    public getMainES2015(@Res() res): any {
        const file = fs.readdirSync(this.rootDir.root).filter(file => (file.match('main\-es2015\.[^\.]*\.js')))[0];
        console.log(`sending ${file}`);
        res.sendFile(file, this.rootDir);
    }
    @Get('polyfills-es5.*.js')
    public getPolyfillsES5(@Res() res): any {
        const file = fs.readdirSync(this.rootDir.root).filter(file => (file.match('polyfills\-es5\.[^\.]*\.js')))[0];
        console.log(`sending ${file}`);
        res.sendFile(file, this.rootDir);
    }
    @Get('polyfills-es2015.*.js')
    public getPolyfillsES2015(@Res() res): any {
        const file = fs.readdirSync(this.rootDir.root).filter(file => (file.match('polyfills\-es2015\.[^\.]*\.js')))[0];
        console.log(`sending ${file}`);
        res.sendFile(file, this.rootDir);
    }
    @Get('runtime-es5.*.js')
    public getRuntimeES5(@Res() res): any {
        const file = fs.readdirSync(this.rootDir.root).filter(file => (file.match('runtime\-es5\.[^\.]*\.js')))[0];
        console.log(`sending ${file}`);
        res.sendFile(file, this.rootDir);
    }
    @Get('runtime-es2015.*.js')
    public getRuntimeES2015(@Res() res): any {
        const file = fs.readdirSync(this.rootDir.root).filter(file => (file.match('runtime\-es2015\.[^\.]*\.js')))[0];
        console.log(`sending ${file}`);
        res.sendFile(file, this.rootDir);
    }
    @Get('styles.*.css')
    public getstyles(@Res() res): any {
        const file = fs.readdirSync(this.rootDir.root).filter(file => (file.match('styles\.[^\.]*\.css')))[0];
        console.log(`sending ${file}`);
        res.sendFile(file, this.rootDir);
    }
}
