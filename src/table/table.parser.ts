import { Injectable } from "@nestjs/common";
import { Table } from "./table.class";
import { PlayerParser } from "./player/player.parser";
import { TableSectionParser } from "./table-section/table-section.parser";

@Injectable()
export class TableParser {

    public constructor(
        private playerParser: PlayerParser,
        private tableSectionParser: TableSectionParser
        ) {}

    public objectToObject(table: Table): any {
        return {
            id: table.id,
            players: this.playerParser.mapToObject(table.players),
            mainSection: this.tableSectionParser.objectToObject(table.mainSection),
            sideSection: this.tableSectionParser.objectToObject(table.sideSection),
            playerSections: this.tableSectionParser.mapToObject(table.playerSections)
        };
    }
}