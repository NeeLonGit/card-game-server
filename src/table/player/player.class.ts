import { Card } from '../card/card.class';
import { CardService } from '../card/card.service';
import { PlayerDto } from './player.dto';
import { CardDto } from '../card/card.dto';

export class Player {
    public name: string;
    public cards: Card[] = [];

    constructor(public readonly id: string){}

    public setName(name: string) {
        this.name = name;
    }

    public update(playerDto: PlayerDto): void {
        this.name = playerDto.name;
        this.cards = playerDto.cards.map((cardDto: CardDto) => new Card(cardDto.suit, cardDto.name));
    }

    public addCard(card: Card): void {
        this.cards.push(card);
    }

    public takeAll(): Card[] {
        return this.cards.splice(0, this.cards.length);
    }

    public take(index: number): Card {
        return this.cards.splice(index, 1)[0];
    }
}