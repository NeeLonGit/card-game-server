import { Injectable } from '@nestjs/common';
import { CreatePlayerDto } from './create-player.dto';
import { Player } from './player.class';
import { IdService } from 'src/id/id.service';

@Injectable()
export class PlayerService {

    constructor(private idService: IdService){}

    createPlayer(createPlayerDto: CreatePlayerDto): Player {
        const id: string = this.idService.createId();
        const player: Player = new Player(id);
        player.setName(createPlayerDto.name);
        console.log('created: ', player);
        return player;
    }
}
