import { Module } from '@nestjs/common';
import { PlayerService } from './player.service';
import { IdModule } from 'src/id/id.module';
import { CardModule } from '../card/card.module';
import { PlayerParser } from './player.parser';

@Module({
  imports: [IdModule, CardModule],
  controllers: [],
  providers: [PlayerService, PlayerParser],
  exports: [PlayerService, PlayerParser]
})
export class PlayerModule {}
