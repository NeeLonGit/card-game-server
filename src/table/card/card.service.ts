import { Injectable } from '@nestjs/common';
import { Card, Suit, CardName } from './card.class';

@Injectable()
export class CardService {

    public static newDeck(): Card[] {
        const suits: Suit[] = [Suit.SPADES, Suit.HEARTS, Suit.CLUBS, Suit.DIAMONDS];
        const values: CardName[] = [CardName.TWO, CardName.THREE, CardName.FOUR, CardName.FIVE, CardName.SIX, CardName.SEVEN, CardName.EIGHT, CardName.NINE, CardName.TEN, CardName.JACK, CardName.QUEEN, CardName.KING, CardName.ACE];
        const deck: Card[] = [];
        for (const suit of suits) {
            for (const value of values) {
                deck.push(new Card(suit, value));
            }
        }
        return deck;
    }
}
