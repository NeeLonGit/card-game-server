import { Player } from './player/player.class';
import { TableSection } from './table-section/table-section.class';
import { Card } from './card/card.class';
import { CardService } from './card/card.service';
import { UpdateTableDto } from './update-table-dto';
import { PlayerDto } from './player/player.dto';
import { TableSectionDto } from './table-section/table-section.dto';

export class Table {

    public players: Map<string, Player> = new Map();
    public mainSection: TableSection = new TableSection();
    public sideSection: TableSection = new TableSection();
    public playerSections: Map<string, TableSection> = new Map();

    constructor(public id: string) {
        const deck: Card[] = CardService.newDeck();
        this.mainSection.addClosedCards(deck);
    }

    public addPlayer(player: Player): void {
        this.playerSections.set(player.id, new TableSection());
        this.players.set(player.id, player);
    }

    public update(updateTableDto: UpdateTableDto): void {
        this.mainSection.update(updateTableDto.mainSection);
        this.sideSection.update(updateTableDto.sideSection);
        this.updatePlayer(updateTableDto.player);
        this.updatePlayerSections(updateTableDto.playerSections);

    }
    public updatePlayer(playerDto: PlayerDto): void {
        if(this.players.has(playerDto.id)) {
            this.players.get(playerDto.id).update(playerDto);
        }
    }

    public removePlayer(playerId: string): void {
        if(this.players.has(playerId)) {
            const player: Player = this.players.get(playerId);
            const cards: Card[] = player.takeAll();
            this.sideSection.addClosedCards(cards);
            
            this.players.delete(playerId);
            this.removePlayerSection(playerId);
        }
    }

    public getPlayerCount(): number {
        return this.players.size;
    }

    public updatePlayerSections(playerSections: { [playerId: string]: TableSectionDto }): void {
        Object.keys(playerSections).forEach( key => this.playerSections.get(key).update(playerSections[key]));
    }

    private emptyPlayerSection(playerId: string): void {
        const openCards: Card[] = this.playerSections.get(playerId).takeAllOpenCards();
        this.sideSection.addOpenCards(openCards);
        const closedCards: Card[] = this.playerSections.get(playerId).takeAllClosedCards();
        this.sideSection.addClosedCards(closedCards);
    }

    private removePlayerSection(playerId: string): void {
        this.emptyPlayerSection(playerId);
        this.playerSections.delete(playerId);
    }
}