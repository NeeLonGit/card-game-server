import { Card } from '../card/card.class';
import { TableSectionDto } from './table-section.dto';
import { CardDto } from '../card/card.dto';

export class TableSection {

    public openCards: Card[] = [];
    public closedCards: Card[] = [];

    public update(tableSectionDto: TableSectionDto): void {
        this.openCards = tableSectionDto.openCards.map((cardDto: CardDto) => new Card(cardDto.suit, cardDto.name));
        this.closedCards = tableSectionDto.closedCards.map((cardDto: CardDto) => new Card(cardDto.suit, cardDto.name));
    }

    public addOpenCards(cards: Card[]): void {
        this.openCards = this.openCards.concat(cards);
    }

    public addClosedCards(cards: Card[]): void {
        this.closedCards = this.closedCards.concat(cards);
    }
    
    public takeAllOpenCards(): Card[] {
        return this.openCards.splice(0, this.openCards.length);
    }
    
    public takeAllClosedCards(): Card[] {
        return this.closedCards.splice(0, this.closedCards.length);
    }

    public takeOneOpenCard(index: number): Card {
        return this.openCards.splice(index, 1)[0];
    }

    public takeOneClosedCard(index: number): Card {
        return this.closedCards.splice(index, 1)[0];
    }
    
    public takeAll(): Card[] {
        return this.takeAllOpenCards().concat(this.takeAllClosedCards());
    }
}