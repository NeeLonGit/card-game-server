import { CardDto } from "../card/card.dto";

export class TableSectionDto {
    public openCards: CardDto[];
    public closedCards: CardDto[];
}