import { Module } from '@nestjs/common';
import { TableSectionParser } from './table-section.parser';
import { CardModule } from '../card/card.module';

@Module({
    imports: [CardModule],
    providers: [TableSectionParser],
    exports: [TableSectionParser]
})
export class TableSectionModule {}
