# Play any card game
_That is: any 4 player card game with a standard 52 cards deck._

## Description

This server is meant to be pretty agnostic about the frontend client, but is setup to serve the Angular card game frontend from my other repository.

## Pre-setup: Angular frontend
Build the Angular frontend:
1. Download the frontend code from [here](https://gitlab.com/NeeLonGit/card-game/-/archive/master/card-game-master.zip) (zip-file).
2. Extract the zip-file somewhere.
3. Open a terminal or command line in the extracted folder.
4. Install the dependencies with `npm install`.
5. Build the angular frontend app with `npm run build`.

You will now have a folder called `dist` with files like `index.html`, `favicon.ico` and 7 files with a long gibberish name.
These files together are the Angular frontend app.
You will serve the frontend app from this backend server.

1. Download the backend code from [here](https://gitlab.com/NeeLonGit/card-game-server/-/archive/master/card-game-server-master.zip) (zip-file).
2. Extract the zip-file somewhere _else_.
3. In the extracted folder, go to the `src` folder.
4. In the `src` folder, create a new file: `app.constants.ts` it should be next to other `app.*.ts` files.
5. In this file, write `export const ANGULAR_ROOT_DIR = "path/to/angular/files";` and replace the path with the actual path to the Angular files from above. It could be something like `"C:/Users/YOURNAME/card-game/kaartspel/dist/kaartspel"`. This path is used in `app.controller.ts` to make the angular app available on `localhost:3000/app` when you run the server.

## Setup
Install the dependencies for the backend.
```bash
$ npm install
```

## Running the app

```bash
$ npm run start
```
The server will keep running until you kill it with `Ctrl+C`. The frontend app is available on http://localhost:3000/app. Enjoy!
